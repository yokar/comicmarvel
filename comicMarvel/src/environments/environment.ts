// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyAzHcx1TJOMrf0T3mcifvdVy9Qa2pNN_cA",
    authDomain: "comicsmarvel-6900c.firebaseapp.com",
    databaseURL: "https://comicsmarvel-6900c.firebaseio.com",
    projectId: "comicsmarvel-6900c",
    storageBucket: "comicsmarvel-6900c.appspot.com",
    messagingSenderId: "463601906446",
    appId: "1:463601906446:web:98d3af1aea8feed24d5d5f"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
