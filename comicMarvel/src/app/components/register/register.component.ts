import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl } from '@angular/forms';
import { ServicesService } from 'src/app/components/services/services.service'
import { Router } from '@angular/router'


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers:[ServicesService],
})
export class RegisterComponent implements OnInit {

  registerForm = new FormGroup({
    email: new FormControl(''),
    password: new FormControl('')
  })

  constructor(private authSvc:ServicesService, private router:Router) { }

  ngOnInit(): void {
  }

  async onRegister(){
    const {email, password } = this.registerForm.value;
    
    try{
      const user = await this.authSvc.register(email, password)
      if(user){
       this.router.navigate(['/comics']);
      }
    }catch(error){
     console.log(error);
    }
  }

}
