import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { ServicesService } from '../services/services.service';
import { Router } from '@angular/router'




@Component({
  selector: 'app-navar',
  templateUrl: './navar.component.html',
  styleUrls: ['./navar.component.css'],
  providers: [ServicesService]
})
export class NavarComponent {

  //public isLogged = false;
 // public user: any;
  public user$: Observable<any> = this.authSvc.afAuth.user;

  constructor(private authSvc:ServicesService, private router: Router) { }

  /*
  async ngOnInit(){
    console.log('Navbar');
    this.user = await this.authSvc.getCurrentUser()
    if(this.user){
      this.isLogged = true;
    }
  }*/

 async onLogout(){
   try{
     alert("Adiós!!!")
    await this.authSvc.logout();
    this.router.navigate(['/comics']);
   }catch(error){
    console.log(error);
   }
  }
}
