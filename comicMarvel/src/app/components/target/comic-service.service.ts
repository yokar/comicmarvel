import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ComicServiceService {

  PUBLIC_KEY = 'ddf887eadc6b01f3dfe09390317d682c2';
  HASH = 'D4CC7BD1F4738F0E72448E5BECA7485E' ;
  //URL_API = `https:gateway.marvel.com:443/v1/public/characters?ts=2&apikey=${this.PUBLIC_KEY}&hash=${this.HASH}`;
  URL_API = `https:gateway.marvel.com/v1/public/comics?ts=1&apikey=${this.PUBLIC_KEY}&hash=${this.HASH}` ;

  constructor( private http: HttpClient ) {  }

    getAllComics () : Observable<any> {
      return this.http.get<any>(this.URL_API)
        .pipe(map((data: any) => data.data.results))
    }

 
}

