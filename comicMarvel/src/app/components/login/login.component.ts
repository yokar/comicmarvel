import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl } from '@angular/forms';
import { ServicesService } from 'src/app/components/services/services.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [ServicesService]
})
export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    email: new FormControl(''),
    password: new FormControl(''),
  })

  constructor(private authSvc:ServicesService, private router: Router) { }

  ngOnInit(): void {
  }

  async onLogin(){
   const {email, password} = this.loginForm.value;

   try{
     const user = await this.authSvc.login(email, password)
     if(user){
      this.router.navigate(['/comics']);
     }else{
       alert('Correo o contraseña incorrectos, si no se ha registrado recapacite y hágalo');
     }
   }catch(error){
    console.log(error);
   }
   
  }

}
